$(document).ready(function() {

    var browserHeight = $(window).height();
    var viewportHeight = $(document).height();
    var sideNav = $('.side-nav');
    var navScrollHidden = (viewportHeight - browserHeight - 400);

    $(window).scroll(function() {
        if ($(document).scrollTop() > navScrollHidden) {
            $(sideNav).addClass("side-nav-off");
        } else {
            $(sideNav).removeClass("side-nav-off");
        }
    });


    size_li_brands = $(".search-list-brands li").size();
    x = 5;
    $('.search-list-brands li:lt('+x+')').show();
    $('#view-more-brands').click(function () {
        x= (x+5 <= size_li_brands) ? x+5 : size_li_brands;
        $('.search-list-brands li:lt('+x+')').show();
    });

    size_li_years = $(".search-list-years li").size();
    x = 5;
    $('.search-list-years li:lt('+x+')').show();
    $('#view-more-years').click(function () {
        x= (x+5 <= size_li_years) ? x+5 : size_li_years;
        $('.search-list-years li:lt('+x+')').show();
    });

})